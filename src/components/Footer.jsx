import React from 'react'
import { Col, Row, Container } from 'react-bootstrap'
import './Footer.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-solid-svg-icons'


const Footer = () => {
    return (
        <>
            <Row className='footer-titles'>
                <Col>
                    <p>Movies</p>
                </Col>
                <Col>
                    <p>Series</p>
                </Col>
                <Col>
                    <p>DisneyLife</p>
                </Col>
                <Col>
                    <p>Holidays</p>
                </Col>
                <Col>
                    <p>Company</p>
                </Col>
            </Row>

            <Row className='footer-base'>
                <Col>
                    <p>Lorem ipsum<br />
                        Excepteur sint occaecat<br />
                        Cupidatat non proident<br />
                        Sunt in<br />
                        Culpa qui officia deserunt<br />
                        Mollit anim id est laborum</p>
                </Col>
                <Col>
                    <p>Lorem ipsum<br />
                        Excepteur sint occaecat<br />
                        Cupidatat non proident<br />
                        Sunt in<br />
                        Culpa qui officia deserunt<br />
                        Mollit anim id est laborum</p>
                </Col>
                <Col>
                    <p>Lorem ipsum<br />
                        Excepteur sint occaecat<br />
                        Cupidatat non proident<br />
                        Sunt in<br />
                        Culpa qui officia deserunt<br />
                        Mollit anim id est laborum</p>
                </Col>
                <Col>
                    <p>Lorem ipsum<br />
                        Excepteur sint occaecat<br />
                        Cupidatat non proident<br />
                        Sunt in<br />
                        Culpa qui officia deserunt<br />
                        Mollit anim id est laborum</p>
                </Col>
                <Col>
                    <p>0206 222 3364<br />
                        hello@disneylife.com<br />
                        press@disney.com
                        <br />
                    </p>
                    <i class="fa-brands fa-instagram" style={{paddingLeft:'40px'}}></i>
                    <i class="fa-brands fa-spotify" style={{paddingLeft:'10px'}}></i>
                    <i class="fa-brands fa-twitter" style={{paddingLeft:'10px'}}></i>


                </Col>


            </Row>


        </>
    )
}

export default Footer
