import React from 'react'
import './Covid.css'

const Covid = () => {
    return (
        <>
            <div className='covid-title'>
                <p>Covid-19 Update</p>

            </div>
            <div className='covid-text'>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum..</p>
            </div>
        </>


    )
}

export default Covid
