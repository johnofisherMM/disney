import React from 'react'
import { Carousel } from 'react-bootstrap'
import banner1 from '../assets/banner1.jpeg'
import banner2 from '../assets/banner2.jpeg'
import banner3 from '../assets/banner3.jpeg'
import starLogo from '../assets/Star_Wars_Yellow_Logo.svg.png'


const BannerSlider = () => {
    return (
        <>
            <Carousel className='carousel-setup' fade>
                <Carousel.Item interval={3000}>
                    <img className="d-block w-100 h-50"
                        src={banner1}
                        alt="First slide" />
                    <Carousel.Caption>
                        <img alt=''
                            style={
                                {
                                    width: '400px',
                                    paddingBottom: '100px'
                                }
                            }
                            className='starwars-logo'
                            src={starLogo} />
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                    <img className="d-block w-100"
                        src={banner2}
                        alt="Second slide" />
                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                    <img className="d-block w-100"
                        src={banner3}
                        alt="Third slide" />
                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>

        </>
    )
}

export default BannerSlider
