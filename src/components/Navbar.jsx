import React from 'react'
import { Nav } from 'react-bootstrap'
import './Navbar.css'
import disneylogo from '../assets/disney-logo.png'

const Navbar = () => {
    return (
        <>
            <Nav className='nav-basic'
                activeKey="/home"
                onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
            >
                <Nav.Item>
                    <Nav.Link href="/home"><img className='disneylogo' alt='test' src={disneylogo}></img></Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link>TV</Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">MOVIES</Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">VIDEO</Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">WIN</Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">HOLIDAYS</Nav.Link>
                </Nav.Item>
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">WIN</Nav.Link>
                </Nav.Item >
                <Nav.Item style={{ paddingTop: '25px' }}>
                    <Nav.Link eventKey="link-2">DISNEYLIFE</Nav.Link>
                </Nav.Item>
            </Nav>
        </>
    )
}

export default Navbar
