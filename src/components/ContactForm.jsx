import React from 'react'
import { Form, Container, Row, Col, DatePicker } from 'react-bootstrap'
import './ContactForm.css'
import Button from './Button'

const ContactForm = () => {
    return (
        <>
            <Container>
                <Row>
                    <p className='contact'>Contact Us</p>
                    <Col lg='7' className="d-flex align-items-center"></Col>
                    <form className="contact-form w-100">
                        <Row>
                            <Col lg='6' className="form-group">
                                <input
                                    className="form-control"
                                    id="name"
                                    name="name"
                                    placeholder='Entering name...'
                                    type='text'
                                />
                            </Col>
                            <Col lg='6' className="form-group">
                                <input
                                    className="form-control"
                                    id="name"
                                    name="name"
                                    placeholder='Surname*'
                                    type='name'
                                />
                            </Col>
                        </Row>
                    </form>
                    <form className="contact-form w-100">
                        <Row>
                            <Col lg='6' className="form-group2">
                                <input
                                    className="form-control"
                                    id="number"
                                    name="number"
                                    placeholder='Contact number'
                                    type='number'
                                />
                            </Col>
                            <Col lg='6' className="form-group2">
                                <input
                                    className="form-control"
                                    id="email"
                                    name="email"
                                    placeholder='Email address*'
                                    type='email'
                                />
                            </Col>
                        </Row>
                        <textarea className='text-area form-control rounded-0' id='message' name='message' placeholder='Leave your message here*' Message row='5'></textarea>
                    </form>
                </Row>
                <Button/>
            </Container>


        </>
    )
}

export default ContactForm
