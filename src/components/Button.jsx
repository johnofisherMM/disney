import React from 'react'
import './Button.css'

const Button = () => {
  return (
    <>
    <button className='button'>Submit <i class="fa-solid fa-arrow-right-long" style={{paddingLeft:'10px'}}></i></button>
    </>
  )
}

export default Button