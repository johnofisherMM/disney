import './App.css';
import BannerSlider from './components/BannerSlider';
import Navbar from './components/Navbar'
import Covid from './components/Covid'
import NewReleases from './components/NewReleases';
import Trailer from './components/Trailer';
//import ShowReel from './components/ShowReel'
//import ContactForm from './components/ContactForm'
import Footer from './components/Footer'
import ContactForm from './components/ContactForm';
import ShowReel from './components/ShowReel';




function App() {
  return (
    <div className="App">
      <Navbar/>
      <BannerSlider/>
      <Covid/>
      <NewReleases/>
      <Trailer/>
      <ShowReel/>
      <ContactForm/>
      <Footer/>
    </div>
  );
}

export default App;
